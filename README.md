# Color generator
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/tadzio%2Fcolor_generator/master)

Color generator simulates perceived color from hyperspectral marine reflectances using CIE color matching functions